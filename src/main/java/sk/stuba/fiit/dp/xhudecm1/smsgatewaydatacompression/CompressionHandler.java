/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sk.stuba.fiit.dp.xhudecm1.smsgatewaydatacompression;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import sk.stuba.fiit.dp.xhudecm1.smsgatewaydatacompression.customalphabet.CustomAlphabetCompressor;
import sk.stuba.fiit.dp.xhudecm1.smsgatewaydatacompression.deflater.Deflater;
import sk.stuba.fiit.dp.xhudecm1.smsgatewaydatacompression.jsonObjects.CustomAlphabet;
import sk.stuba.fiit.dp.xhudecm1.smsgatewaydatacompression.shoco.Shoco;
import sk.stuba.fiit.dp.xhudecm1.smsgatewaydatacompression.smaz.Smaz;
import sk.stuba.fiit.dp.xhudecm1.smsgatewaysmshandler.smshandler.SmsSecurePDU;

/**
 *
 * @author martinhudec
 */
public abstract class CompressionHandler {

    private final Shoco shocoInstance;
    private final Smaz smazInstance;
    private final Deflater deflaterInstance;
    private final CustomAlphabetCompressor customAlphabetCompressorInstance;
    final static org.apache.log4j.Logger LOG = org.apache.log4j.Logger.getLogger(CompressionHandler.class);

    public CompressionHandler() {
        shocoInstance = new Shoco();
        smazInstance = new Smaz();
        deflaterInstance = new Deflater();
        customAlphabetCompressorInstance = new CustomAlphabetCompressor();

    }

    public CompressedData compressDataSpecific(byte[] data, CustomAlphabet customAlphabet, SmsSecurePDU.CompressionTypeEnum compressionType) throws IOException {
        LOG.debug("original_length " + data.length);
        switch (compressionType) {
            case NONE:
                return new CompressedData(data, SmsSecurePDU.CompressionTypeEnum.NONE, data.length);
            case CUSTOM_ALPHABET:
                byte[] customAlphabetCompressed = customAlphabetCompressorInstance.customAlphabetCompress(data, customAlphabet);
                LOG.debug("custom alphabet compression " + customAlphabetCompressed.length + " " + new String(customAlphabetCompressed));
                return new CompressedData(customAlphabetCompressed, SmsSecurePDU.CompressionTypeEnum.CUSTOM_ALPHABET, customAlphabetCompressed.length);
            case DEFLATER:
                byte[] deflatorCompressed = deflaterInstance.deflaterCompress(data);
                LOG.debug("deflato_compression " + deflatorCompressed.length + " " + new String(deflatorCompressed));
                return new CompressedData(deflatorCompressed, SmsSecurePDU.CompressionTypeEnum.DEFLATER, deflatorCompressed.length);
            case SHOCO:
                byte[] shocoCompressed = shocoInstance.shoco_compress(data, data.length);
                LOG.debug("shoco_compression " + shocoCompressed.length + " " + new String(shocoCompressed));
                return new CompressedData(shocoCompressed, SmsSecurePDU.CompressionTypeEnum.SHOCO, shocoCompressed.length);
            case SMAZ:
                byte[] smazCompressed = smazInstance.smaz_compress(data, data.length);
                LOG.debug("smaz_compression " + smazCompressed.length + " " + new String(smazCompressed));
                return new CompressedData(smazCompressed, SmsSecurePDU.CompressionTypeEnum.SMAZ, smazCompressed.length);
            default:
                return null;
        }
    }

    public CompressedData compressData(byte[] data, CustomAlphabet customAlphabet) throws IOException {
        List<CompressedData> compressedDataList = new ArrayList<>();
        byte[] shocoCompressed = shocoInstance.shoco_compress(data, data.length);
        byte[] smazCompressed = smazInstance.smaz_compress(data, data.length);
        byte[] deflatorCompressed = deflaterInstance.deflaterCompress(data);
        byte[] shocoDeflaterCompressed = deflaterInstance.deflaterCompress(shocoCompressed);
        byte[] smazDeflaterCompressed = deflaterInstance.deflaterCompress(smazCompressed);
        byte[] customAlphabetCompressed = null;
        if (customAlphabet != null) {
            customAlphabetCompressed = customAlphabetCompressorInstance.customAlphabetCompress(data, customAlphabet);
            LOG.debug("custom alphabet compression " + customAlphabetCompressed.length + " " + new String(customAlphabetCompressed));
            compressedDataList.add(new CompressedData(customAlphabetCompressed, SmsSecurePDU.CompressionTypeEnum.CUSTOM_ALPHABET, customAlphabetCompressed.length));
        }
        LOG.debug("shoco_compression " + shocoCompressed.length + " " + new String(shocoCompressed));
        LOG.debug("shoco deflater compression " + shocoDeflaterCompressed.length + "" + new String(shocoDeflaterCompressed));

        LOG.debug("smaz_compression " + smazCompressed.length + " " + new String(smazCompressed));
        LOG.debug("smaz deflater compression " + smazDeflaterCompressed.length + " " + new String(smazDeflaterCompressed));
        LOG.debug("deflato_compression " + deflatorCompressed.length + " " + new String(deflatorCompressed));
        LOG.debug("original_length " + data.length);
        compressedDataList.add(new CompressedData(smazCompressed, SmsSecurePDU.CompressionTypeEnum.SMAZ, smazCompressed.length));
        compressedDataList.add(new CompressedData(shocoCompressed, SmsSecurePDU.CompressionTypeEnum.SHOCO, shocoCompressed.length));
        compressedDataList.add(new CompressedData(deflatorCompressed, SmsSecurePDU.CompressionTypeEnum.DEFLATER, deflatorCompressed.length));
        compressedDataList.add(new CompressedData(data, SmsSecurePDU.CompressionTypeEnum.NONE, data.length));
        compressedDataList.add(new CompressedData(smazDeflaterCompressed, SmsSecurePDU.CompressionTypeEnum.SMAZ_DEFLATER, smazDeflaterCompressed.length));
        compressedDataList.add(new CompressedData(shocoDeflaterCompressed, SmsSecurePDU.CompressionTypeEnum.SHOCO_DEFLATER, shocoDeflaterCompressed.length));
        Collections.sort(compressedDataList, (a, b) -> a.getCompressedLength().compareTo(b.getCompressedLength()));
        return compressedDataList.get(0);

    }

    public byte[] decompressData(byte[] data, SmsSecurePDU.CompressionTypeEnum compressionType, CustomAlphabet customAlphabet) throws IOException {
        switch (compressionType) {
            case NONE:
                LOG.debug("NO compression were set");
                return data;
            case SHOCO:
                byte[] shocodecompressed = shocoInstance.shoco_decompress(data, data.length);
                LOG.debug("shoco_decompression original data length " + data.length + " decompressed data " + new String(shocodecompressed));
                return shocodecompressed;
            case SMAZ:
                byte[] smazdecompressed = smazInstance.smaz_decompress(data, data.length);
                LOG.debug("smaz_dcompression original data length" + data.length + " decompressed data " + new String(smazdecompressed));
                return smazdecompressed;
            case DEFLATER:
                byte[] deflatordecompressed = deflaterInstance.deflaterDeCompress(data);
                LOG.debug("deflator decompression original data length" + data.length + " decompressed data " + new String(deflatordecompressed));
                return deflatordecompressed;
            case SHOCO_DEFLATER:
                byte[] shocodeflaterdecompressed = deflaterInstance.deflaterDeCompress(data);
                byte[] shocodecomp = shocoInstance.shoco_decompress(shocodeflaterdecompressed, shocodeflaterdecompressed.length);
                LOG.debug("shocodeflaterdecompressed decompression original data length" + data.length + " decompressed data " + new String(shocodecomp));
                return shocodecomp;
            case SMAZ_DEFLATER:
                byte[] smazdeflaterdecompressed = deflaterInstance.deflaterDeCompress(data);
                byte[] smazdecomp = smazInstance.smaz_decompress(smazdeflaterdecompressed, smazdeflaterdecompressed.length);
                LOG.debug("smazdeflaterdecompressed decompression original data length" + data.length + " decompressed data " + new String(smazdecomp));
                return smazdecomp;
            case CUSTOM_ALPHABET:
                if (customAlphabet != null) {
                    byte[] customalphabetdecompressed = customAlphabetCompressorInstance.customAlphabetDecompress(data, customAlphabet);
                    LOG.debug("cusom alphabet decompression original data length" + data.length + " decompressed data " + new String(customalphabetdecompressed));
                    return customalphabetdecompressed;
                } else {
                    throw new UnsupportedOperationException(compressionType + " compression type is not supported yet no custom alphabet");
                }
            default:
                throw new UnsupportedOperationException(compressionType + " compression type is not supported yet");
        }
    }

    public String generateCustomAlphabetJson(String dataSet) {
        String deletedPunctuation = dataSet.replaceAll("[\\{\\}\\,\\.\\(\\)\\[\\]\\+\\-\\:\\;\\\"\\\'\\/]", " ");
        String deletedNumbers = deletedPunctuation.replaceAll("\\b(\\d*)\\b", " ");
        String variablesOnly = deletedNumbers.replaceAll("\\s\\s+", ":");
        String removedStartingAndtrailingChar = variablesOnly.replaceAll("^:|:$", "");
        String variables[] = removedStartingAndtrailingChar.split(":");

        BiMap<Integer, String> map = HashBiMap.create();

        List<String> variablesList = Arrays.asList(variables);
        Integer iterator = 0;
        for (String var : variablesList) {
            if (map.containsValue(var)) {
                continue;
            }
            map.put(iterator, var);
            iterator++;
        }
        Map<Integer, String> customAlphabetMap = new HashMap<>();
        customAlphabetMap.putAll(map);
        CustomAlphabet alphabet = new CustomAlphabet(customAlphabetMap);

        Gson gson = new GsonBuilder()
                .disableHtmlEscaping()
                .setFieldNamingPolicy(FieldNamingPolicy.UPPER_CAMEL_CASE)
                .setPrettyPrinting()
                .serializeNulls()
                .create();

        String dataJson = gson.toJson(alphabet);
        return dataJson;
    }

}
