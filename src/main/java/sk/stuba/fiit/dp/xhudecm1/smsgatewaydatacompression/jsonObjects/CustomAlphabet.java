/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sk.stuba.fiit.dp.xhudecm1.smsgatewaydatacompression.jsonObjects;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import java.util.Map;

/**
 *
 * @author martinhudec
 */
public class CustomAlphabet {

    private Map<Integer, String> alphabetMap;

    public CustomAlphabet(Map<Integer, String> alphabetMap) {
        this.alphabetMap = alphabetMap;
    }

    public BiMap<Integer, String> getAlphabetMap() {
        BiMap<Integer, String> bidimap = HashBiMap.create();
        bidimap.putAll(alphabetMap);
        return bidimap;
    }

    public void setAlphabetMap(Map<Integer, String> alphabetMap) {
        this.alphabetMap = alphabetMap;
    }

    @Override
    public String toString() {
        return "Configuration [alphabet=" + alphabetMap.toString() + "]";
    }
}
