/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sk.stuba.fiit.dp.xhudecm1.smsgatewaydatacompression.customalphabet;

import java.io.IOException;
import java.util.Map;
import sk.stuba.fiit.dp.xhudecm1.smsgatewaydatacompression.CompressionHandler;
import sk.stuba.fiit.dp.xhudecm1.smsgatewaydatacompression.deflater.Deflater;
import sk.stuba.fiit.dp.xhudecm1.smsgatewaydatacompression.jsonObjects.CustomAlphabet;

/**
 *
 * @author martinhudec
 */
public class CustomAlphabetCompressor {

    
    private final Deflater deflater;
    private Boolean client = false;
    final static org.apache.log4j.Logger LOG = org.apache.log4j.Logger.getLogger(CustomAlphabetCompressor.class);

    public CustomAlphabetCompressor() {

        deflater = new Deflater();
    }

    public byte[] customAlphabetCompress(byte[] data, CustomAlphabet customAlphabet) throws IOException {
        String dataString = new String(data);
        for (Map.Entry<Integer, String> entry : customAlphabet.getAlphabetMap().entrySet()) {
            Integer key = entry.getKey();
            String value = entry.getValue();
            dataString = dataString.replace(value, "_" + key + "_");
        }

        return deflater.deflaterCompress(dataString.getBytes());
    }

    public byte[] customAlphabetDecompress(byte[] data, CustomAlphabet customAlphabet) throws IOException {
        String dataString = new String(deflater.deflaterDeCompress(data));
        for (Map.Entry<Integer, String> entry : customAlphabet.getAlphabetMap().entrySet()) {
            Integer key = entry.getKey();
            String value = entry.getValue();
            dataString = dataString.replace("_" + key + "_", value);
        }

        return dataString.getBytes();
    }

}
