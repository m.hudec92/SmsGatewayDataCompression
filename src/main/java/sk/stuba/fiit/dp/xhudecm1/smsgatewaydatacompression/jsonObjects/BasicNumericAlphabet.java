/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sk.stuba.fiit.dp.xhudecm1.smsgatewaydatacompression.jsonObjects;
import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import com.google.gson.internal.LinkedTreeMap;
import java.util.Map;

/**
 *
 * @author martinhudec
 */
public class BasicNumericAlphabet {
//BiMap<String, String> biMap = HashBiMap.create();
    private LinkedTreeMap<Character, Integer> alphabetMap;

    public BasicNumericAlphabet(LinkedTreeMap<Character, Integer> alphabetMap) {
        this.alphabetMap = alphabetMap;
    }

    public BiMap<Character, Integer> getAlphabetMap() {
        BiMap<Character, Integer> bidimap = HashBiMap.create();
        bidimap.putAll(alphabetMap);
        return bidimap;
    }

    public void setAlphabetMap(LinkedTreeMap<Character, Integer> alphabetMap) {
        this.alphabetMap = alphabetMap;
    }

    public static class Alphabet {

        private static String alphabet = "{\"AlphabetMap\": {\n"
                + "  \"0\":0,\n"
                + "  \"1\":1,\n"
                + "  \"2\":2,\n"
                + "  \"3\":3,\n"
                + "  \"4\":4,\n"
                + "  \"5\":5,\n"
                + "  \"6\":6,\n"
                + "  \"7\":7,\n"
                + "  \"8\":8,\n"
                + "  \"9\":9,\n"
                + "  \"(\":10,\n"
                + "  \")\":11,\n"
                + "  \"[\":12,\n"
                + "  \"]\":13,\n"
                + "  \"{\":14,\n"
                + "  \"}\":15,\n"
                + "  \"-\":16,\n"
                + "  \".\":17,\n"
                + "  \",\":18,\n"
                + "  \":\":19,\n"
                + "  \";\":20,\n"
                + "  \"?\":21,\n"
                + "  \"!\":22,\n"
                + "  \"_\":23,\n"
                + "  \"%\":24,\n"
                + "  \">\":25,\n"
                + "  \"<\":26,\n"
                + "  \"=\":27,\n"
                + "  \"&\":28,\n"
                + "  \"*\":29,\n"
                + "  \"\\\"\":30,\n"
                + "  \"@\":31\n"
                + "  }\n"
                + "}";

        public static String getAlphabet() {
            return alphabet;
        }

    }

    @Override
    public String toString() {
        return "Configuration [alphabet=" + alphabetMap.toString() + "]";
    }
}
