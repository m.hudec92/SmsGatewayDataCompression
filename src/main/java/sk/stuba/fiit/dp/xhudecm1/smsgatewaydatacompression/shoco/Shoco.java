/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sk.stuba.fiit.dp.xhudecm1.smsgatewaydatacompression.shoco;

import cz.adamh.utils.NativeUtils;
import java.io.IOException;

/**
 *
 * @author martinhudec
 */
public class Shoco {

    static {
        String systemArchitecture = System.getProperty("os.arch");
        //System.load("/opt/smsGatewayClientLibraryData/binaries/libShocoLibraryWrapper.so");
        
        try {
            if (systemArchitecture.equals("arm")) {
                NativeUtils.loadLibraryFromJar("/pi/libShocoLibraryWrapper.so");
            } else {
                NativeUtils.loadLibraryFromJar("/linux/libShocoLibraryWrapper.so");
            }
        } catch (IOException e) {
            // This is probably not the best way to handle exception :-)    
            e.printStackTrace();
        }
         
    }

    public Shoco() {
    }

    public native byte[] shoco_compress(byte[] input, int length);

    public native byte[] shoco_decompress(byte[] input, int length);
}
