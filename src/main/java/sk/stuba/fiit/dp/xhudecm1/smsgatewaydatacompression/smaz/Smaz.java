/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sk.stuba.fiit.dp.xhudecm1.smsgatewaydatacompression.smaz;

import cz.adamh.utils.NativeUtils;
import java.io.IOException;

/**
 *
 * @author martinhudec
 */
public class Smaz {

    static {
        String systemArchitecture = System.getProperty("os.arch");
        //System.out.println(systemArchitecture);
        //System.load("/opt/smsGatewayClientLibraryData/binaries/libSmazLibraryWrapper.so");

        try {

            if (systemArchitecture.equals("arm")) {
                NativeUtils.loadLibraryFromJar("/pi/libSmazLibraryWrapper.so");
            } else {
                NativeUtils.loadLibraryFromJar("/linux/libSmazLibraryWrapper.so");
            }
        } catch (IOException e) {
            // This is probably not the best way to handle exception :-)    
            e.printStackTrace();
        }

    }

    public Smaz() {
    }

    public native byte[] smaz_compress(byte[] input, int length);

    public native byte[] smaz_decompress(byte[] input, int length);
}
