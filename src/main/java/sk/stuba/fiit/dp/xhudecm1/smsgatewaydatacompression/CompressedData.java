/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sk.stuba.fiit.dp.xhudecm1.smsgatewaydatacompression;

import sk.stuba.fiit.dp.xhudecm1.smsgatewaysmshandler.smshandler.SmsSecurePDU;

/**
 *
 * @author martinhudec
 */
public class CompressedData {
        private final byte[] compressed;
    private final SmsSecurePDU.CompressionTypeEnum compressionType;
    private final Integer compressedLength;

    public CompressedData(byte[] compressed, SmsSecurePDU.CompressionTypeEnum compressionType, Integer compressedLength) {
        this.compressed = compressed;
        this.compressionType = compressionType;
        this.compressedLength = compressedLength;
    }


    public Integer getCompressedLength() {
        return compressedLength;
    }

    public byte[] getCompressed() {
        return compressed;
    }

    public SmsSecurePDU.CompressionTypeEnum getCompressionType() {
        return compressionType;
    }
}
