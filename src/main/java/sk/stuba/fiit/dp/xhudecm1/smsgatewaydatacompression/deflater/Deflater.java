/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sk.stuba.fiit.dp.xhudecm1.smsgatewaydatacompression.deflater;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.zip.DeflaterOutputStream;
import java.util.zip.InflaterInputStream;

/**
 *
 * @author martinhudec
 */
public class Deflater {

    public Deflater() {
    }

    public byte[] deflaterCompress(byte[] data) throws IOException {
        if (data == null || data.length == 0) {
            return data;
        }

        ByteArrayInputStream input = new ByteArrayInputStream(data);
        ByteArrayOutputStream output = new ByteArrayOutputStream(data.length / 2);

        DeflaterOutputStream deflater = new DeflaterOutputStream(output);
            
        int i;
        while ((i = input.read()) != -1) {
            deflater.write((byte) i);
            deflater.flush();
        }
        input.close();
        output.close();
        deflater.close();

        return output.toByteArray();
    }

    public byte[] deflaterDeCompress(byte[] data) throws IOException {
        ByteArrayInputStream input = new ByteArrayInputStream(data);
        ByteArrayOutputStream output = new ByteArrayOutputStream(data.length / 2);

        InflaterInputStream inflater = new InflaterInputStream(input);

        int i;
        while ((i = inflater.read()) != -1) {
            output.write((byte) i);
            output.flush();
        }
        input.close();
        output.close();
        inflater.close();

        return output.toByteArray();
    }
}
